package com.refactorlabs.cs378.assign8;

import com.refactorlabs.cs378.sessions.Event;
import com.refactorlabs.cs378.sessions.EventType;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroMultipleOutputs;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/* Robert Gehring (UTEid: rjg2358)(CSUsername: rgehring) */
/* Big Data Programming - Spring 2015 - David Franke     */
/* University of Texas at Austin                         */

public class UserSessionsFilterMapper extends Mapper<AvroKey<CharSequence>,AvroValue<Session>, AvroKey<CharSequence>, AvroValue<Session>>
{
    private static final String MAPPER_COUNTER_GROUP = "Mapper Counts";
    private AvroMultipleOutputs multipleOutputs;

    public void map(AvroKey<CharSequence> key, AvroValue<Session> value, Context context) throws IOException, InterruptedException
    {
        String userClassification = getUserClassification(value.datum()); /* Get type of user */

        if (userClassification!=null) /* If not more than 1000 events in a session */
        {
            context.getCounter(MAPPER_COUNTER_GROUP, userClassification).increment(1L);

            multipleOutputs.write("sessionType", key, value, userClassification);
        }
        else
            context.getCounter(MAPPER_COUNTER_GROUP, "Thrown Away Sessions").increment(1L);
    }

    /* init the multiple outputs handle */
    public void setup(Context context)
    {
        this.multipleOutputs = new AvroMultipleOutputs(context);
    }

    /* flush out the output */
    public void cleanup(Context context) throws InterruptedException, IOException
    {
        multipleOutputs.close();
    }

    /* Will return what kind of user we have. Will return null if this session has more than 1k events */
    public static String getUserClassification(Session s)
    {
        /*
        Submitter - session has any of these events: CHANGE, CONTACT_FORM_STATUS, EDIT, SUBMIT
        Sharer - Not a submitter, has a SHARE event
        Clicker - Not a Sharer, has a CLICK event
        Shower - Not a Clicker, has a SHOW event
        Visitor - Has only VISIT events
        Other - None of the above (are there any of these?)
        */
        if(s.getEvents().size() > 1000)
            return null;
        else if (isSubmitter(s))
            return "Submitter";
        else if (isSharer(s))
            return "Sharer";
        else if (isClicker(s))
            return "Clicker";
        else if (isShower(s))
            return "Shower";
        else if (isVisitor(s))
            return "Visitor";
        return "Other";
    }

    /* If this user is a submitter it will return true */
    private static boolean isSubmitter(Session s)
    {
        for(Event e: s.getEvents())
        {
            if(e.getEventType().equals(EventType.CHANGE) || e.getEventType().equals(EventType.CONTACT_FORM_STATUS) || e.getEventType().equals(EventType.EDIT) || e.getEventType().equals(EventType.SUBMIT))
                return true;
        }
        return false;
    }

    /* If this user is a sharer it will return true */
    private static boolean isSharer(Session s)
    {
        for(Event e: s.getEvents())
        {
            if(e.getEventType().equals(EventType.SHARE))
                return true;
        }
        return false;
    }

    /* If this user is a clicker it will return true */
    private static boolean isClicker(Session s)
    {
        for(Event e: s.getEvents())
        {
            if(e.getEventType().equals(EventType.CLICK))
                return true;
        }
        return false;
    }

    /* If this user is a shower it will return true */
    private static boolean isShower(Session s)
    {
        for(Event e: s.getEvents())
        {
            if(e.getEventType().equals(EventType.SHOW))
                return true;
        }
        return false;
    }

    /* If this user is a visitor it will return true */
    private static boolean isVisitor(Session s)
    {
        for(Event e: s.getEvents())
        {
            if(!e.getEventType().equals(EventType.VISIT))
                return false;
        }
        return true;
    }
}
