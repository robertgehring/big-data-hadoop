package com.refactorlabs.cs378.assign8;


import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.Schema;
import org.apache.avro.mapreduce.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URL;
import java.net.URLClassLoader;

/* Robert Gehring (UTEid: rjg2358)(CSUsername: rgehring) */
/* Big Data Programming - Spring 2015 - David Franke     */
/* University of Texas at Austin                         */

public class UserSessionsFilterDriver extends Configured implements Tool
{
    /**
     * The run() method is called (indirectly) from main(), and contains all the job
     * setup and configuration.
     */
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            System.err.println("Usage: junk.ReduceSideJoinDriver <input path> <output path>");
            return -1;
        }

        System.out.println("User Sessions Filter Driver start");

        Configuration conf = getConf();
        Job job = new Job(conf, "UserSessionFilter");
        String[] appArgs = new GenericOptionsParser(conf, args).getRemainingArgs();

        // Identify the JAR file to replicate to all machines.
        job.setJarByClass(UserSessionsFilterDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf.set("mapreduce.user.classpath.first", "true");


        // Specify the input schema
        AvroJob.setInputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setInputValueSchema(job, Session.getClassSchema());


        // Specify the map
        job.setInputFormatClass(AvroKeyValueInputFormat.class);
        job.setMapperClass(UserSessionsFilterMapper.class);

        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);

        AvroJob.setMapOutputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setMapOutputValueSchema(job, Session.getClassSchema());

        AvroMultipleOutputs.addNamedOutput(job, "sessionType", AvroKeyValueOutputFormat.class, Schema.create(Schema.Type.STRING), Session.getClassSchema());
        AvroMultipleOutputs.setCountersEnabled(job, true);


        // Grab the input file and output directory from the command line.
        String[] inputPaths = appArgs[0].split(",");
        for (String inputPath : inputPaths) {
            FileInputFormat.addInputPath(job, new Path(inputPath));
        }

        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));

        // Initiate the map-reduce job, and wait for completion.
        job.waitForCompletion(true);

        return 0;
    }

    private static void printClassPath() {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        URL[] urls = ((URLClassLoader) cl).getURLs();
        System.out.println("classpath BEGIN");
        for (URL url : urls) {
            System.out.println(url.getFile());
        }
        System.out.println("classpath END");

    }

    /**
     * The main method specifies the characteristics of the map-reduce job
     * by setting values on the Job object, and then initiates the map-reduce
     * job and waits for it to complete.
     */
    public static void main(String[] args) throws Exception {
        printClassPath();
            /* Purpose: custom args here */
            /* Configuration object created will be used in run. */
            /* new WordStatistics object will get run called on it with args */
            /* args are the parameters sent in from the cluster def */
        int res = ToolRunner.run(new Configuration(), new UserSessionsFilterDriver(), args);
        System.exit(res);
    }
}
