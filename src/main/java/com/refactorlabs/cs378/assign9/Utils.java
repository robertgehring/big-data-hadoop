package com.refactorlabs.cs378.assign9;

import com.refactorlabs.cs378.sessions.EventSubtype;

import java.util.Map;

public class Utils
{
    public static int NUMBER_OF_EVENT_SUBTYPES = 17;

    public static void populateKeys(Map<EventSubtype, Long> map)
    {
        for(EventSubtype e: EventSubtype.values())
            map.put(e, 0L);
    }

    public static void  increment(Map<EventSubtype, Long> map, EventSubtype key)
    {
        if(map.containsKey(key))
        {
            map.put(key, map.get(key) + 1L);
        }
        else
        {
            throw new UnknownError("Shouldn't have made it here. All keys should be defined");
        }
    }
}
