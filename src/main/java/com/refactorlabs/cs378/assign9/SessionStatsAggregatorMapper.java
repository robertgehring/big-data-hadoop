package com.refactorlabs.cs378.assign9;

import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/* Robert Gehring (UTEid: rjg2358)           */
/* University of Texas, Austin - Spring 2015 */
/* Big Data Programming, David Franke        */
public class SessionStatsAggregatorMapper extends Mapper<AvroKey<ClickSubtypeStatisticsKey>, AvroValue<ClickSubtypeStatisticsData>,
        AvroKey<ClickSubtypeStatisticsKey>, AvroValue<ClickSubtypeStatisticsData>>
{
    public void map(AvroKey<ClickSubtypeStatisticsKey> key, AvroValue<ClickSubtypeStatisticsData> value,
                    Context context) throws IOException, InterruptedException
    {
        /* For each session, every click type */
        ClickSubtypeStatisticsKey.Builder copyKey = ClickSubtypeStatisticsKey.newBuilder();
        copyKey.setSessionType(key.datum().getSessionType());
        copyKey.setClickSubtype(key.datum().getClickSubtype());

        /* Clicker tats for any session type */
        ClickSubtypeStatisticsKey.Builder anySessionTypeKey = ClickSubtypeStatisticsKey.newBuilder();
        anySessionTypeKey.setSessionType("any");
        anySessionTypeKey.setClickSubtype( key.datum().getClickSubtype() );

        /* Clicker stats for each session type buy any click type */
        ClickSubtypeStatisticsKey.Builder anySubEventKey = ClickSubtypeStatisticsKey.newBuilder();
        anySubEventKey.setSessionType( key.datum().getSessionType() );
        anySubEventKey.setClickSubtype("any");

        /* Clicker stats for any session type and any click type */
        ClickSubtypeStatisticsKey.Builder anyAnyKey = ClickSubtypeStatisticsKey.newBuilder();
        anyAnyKey.setSessionType("any");
        anyAnyKey.setClickSubtype("any");

        ClickSubtypeStatisticsData.Builder copyValue = ClickSubtypeStatisticsData.newBuilder();
        copyValue.setSessionCount( value.datum().getSessionCount() );
        copyValue.setTotalCount( value.datum().getTotalCount() );
        copyValue.setSumOfSquares( value.datum().getSumOfSquares() );
        copyValue.setMean( value.datum().getMean() );
        copyValue.setVariance( value.datum().getVariance() );


        /* The key each writes out corresponds to what statistic it contributes too */
        context.write(new AvroKey<ClickSubtypeStatisticsKey>(copyKey.build()),
                new AvroValue<ClickSubtypeStatisticsData>(copyValue.build()));

        context.write(new AvroKey<ClickSubtypeStatisticsKey>(anySessionTypeKey.build()),
                new AvroValue<ClickSubtypeStatisticsData>(copyValue.build()));

        context.write(new AvroKey<ClickSubtypeStatisticsKey>(anySubEventKey.build()),
                new AvroValue<ClickSubtypeStatisticsData>(copyValue.build()));

        context.write(new AvroKey<ClickSubtypeStatisticsKey>(anyAnyKey.build()),
                new AvroValue<ClickSubtypeStatisticsData>(copyValue.build()));
    }
}
