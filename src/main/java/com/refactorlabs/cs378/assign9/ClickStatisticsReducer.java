package com.refactorlabs.cs378.assign9;


import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;

/* Robert Gehring (UTEid: rjg2358)           */
/* University of Texas, Austin - Spring 2015 */
/* Big Data Programming, David Franke        */

public class ClickStatisticsReducer extends Reducer<AvroKey<ClickSubtypeStatisticsKey>, AvroValue<ClickSubtypeStatisticsData>, AvroKey<ClickSubtypeStatisticsKey>, AvroValue<ClickSubtypeStatisticsData>>
{
    private static final String REDUCER_COUNTER_GROUP = "Reducer Counts";
    public void reduce(AvroKey<ClickSubtypeStatisticsKey> key, Iterable<AvroValue<ClickSubtypeStatisticsData>> values, Context context) throws IOException, InterruptedException
    {
        long sCounts = 0L;
        long tCounts = 0L;
        long sOfSquares = 0L;

        /* Add up all the stats */
        for(AvroValue<ClickSubtypeStatisticsData> valueWrapped: values)
        {
            ClickSubtypeStatisticsData value = valueWrapped.datum();

            sCounts = sCounts + value.getSessionCount();
            tCounts = tCounts + value.getTotalCount();
            sOfSquares = sOfSquares + value.getSumOfSquares();
        }

        ClickSubtypeStatisticsKey.Builder keyCopy = ClickSubtypeStatisticsKey.newBuilder();
        keyCopy.setSessionType( key.datum().getSessionType() );
        keyCopy.setClickSubtype( key.datum().getClickSubtype() );

        /* Set the values and computer/set the mean and variance */
        ClickSubtypeStatisticsData.Builder clickAggregator = ClickSubtypeStatisticsData.newBuilder();

        if(keyCopy.getClickSubtype() == "any") /* For the extra credit */
            clickAggregator.setSessionCount( sCounts / Utils.NUMBER_OF_EVENT_SUBTYPES );
        else
            clickAggregator.setSessionCount( sCounts );

        clickAggregator.setTotalCount( tCounts );
        clickAggregator.setSumOfSquares( sOfSquares );
        clickAggregator.setMean( 1.0*tCounts / clickAggregator.getSessionCount() );
        clickAggregator.setVariance( sOfSquares*1.0/clickAggregator.getSessionCount() - clickAggregator.getMean()*1.0*clickAggregator.getMean());


        context.getCounter(REDUCER_COUNTER_GROUP, "Write Outs").increment(1L);
        context.write(new AvroKey<ClickSubtypeStatisticsKey>(keyCopy.build()),
                new AvroValue<ClickSubtypeStatisticsData>(clickAggregator.build()));

    }
}
