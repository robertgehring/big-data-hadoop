package com.refactorlabs.cs378.assign9;

import com.refactorlabs.cs378.assign8.UserSessionsFilterMapper;
import com.refactorlabs.cs378.sessions.Event;
import com.refactorlabs.cs378.sessions.EventSubtype;
import com.refactorlabs.cs378.sessions.EventType;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.mapreduce.Mapper;
import java.io.IOException;
import java.util.HashMap;

/* Robert Gehring (UTEid: rjg2358)           */
/* University of Texas, Austin - Spring 2015 */
/* Big Data Programming, David Franke        */

public class ClickStatisticsMapper extends Mapper<AvroKey<CharSequence>, AvroValue<Session>,
        AvroKey<ClickSubtypeStatisticsKey>, AvroValue<ClickSubtypeStatisticsData>>
{
    private static final String MAPPER_COUNTER_GROUP = "Mapper Counts";

    public void map(AvroKey<CharSequence> key, AvroValue<Session> value, Context context) throws IOException, InterruptedException
    {
        HashMap<EventSubtype, Long> clickCountsMap = new HashMap<EventSubtype, Long>();
        Utils.populateKeys(clickCountsMap); //all of the event subtypes with count 0

        /* Add up the number of times each subtype was seen */
        for(Event e: value.datum().getEvents()) {
            if (e.getEventType() == EventType.CLICK)
            {
                context.getCounter(MAPPER_COUNTER_GROUP, "click occurances").increment(1L);
                Utils.increment(clickCountsMap, e.getEventSubtype());
            }
        }

        /* For each event subtype output a builder with the stats */
        for(EventSubtype eSubtype: EventSubtype.values())
        {
            ClickSubtypeStatisticsKey.Builder statsKeyBuilder = ClickSubtypeStatisticsKey.newBuilder();
            statsKeyBuilder.setSessionType(UserSessionsFilterMapper.getUserClassification(value.datum()));
            statsKeyBuilder.setClickSubtype(eSubtype.toString());

            ClickSubtypeStatisticsData.Builder statsDataBuilder = ClickSubtypeStatisticsData.newBuilder();
            statsDataBuilder.setTotalCount( clickCountsMap.get(eSubtype) );
            statsDataBuilder.setSessionCount( 1L );
            statsDataBuilder.setSumOfSquares( clickCountsMap.get(eSubtype) * clickCountsMap.get(eSubtype) );
            statsDataBuilder.setMean( 0.0 );
            statsDataBuilder.setVariance( 0.0 );

            context.getCounter(MAPPER_COUNTER_GROUP, "Write Outs").increment(1L);
            context.write(new AvroKey<ClickSubtypeStatisticsKey>(statsKeyBuilder.build()),
                        new AvroValue<ClickSubtypeStatisticsData>(statsDataBuilder.build()));
        }
    }


}
