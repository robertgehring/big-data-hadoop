package com.refactorlabs.cs378.assign9;

import com.refactorlabs.cs378.assign8.UserSessionsFilterMapper;
import com.refactorlabs.cs378.sessions.Session;
import org.apache.avro.Schema;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyValueInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.avro.mapreduce.AvroMultipleOutputs;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URL;
import java.net.URLClassLoader;

/* Robert Gehring (UTEid: rjg2358)           */
/* University of Texas, Austin - Spring 2015 */
/* Big Data Programming, David Franke        */

public class SessionStatisticsJobChainDriver extends Configured implements Tool {


    public int run(String[] args) throws Exception
    {
        if (args.length != 2) {
            System.err.println("Usage: junk.SessionsStatisticsJobChainDriver <input path> <output path>");
            return -1;
        }

        /* START****************************** Binning Job ******************************START */
        Configuration conf1 = getConf();
        Job job = new Job(conf1, "UserSessionFilter");
        String[] appArgs = new GenericOptionsParser(conf1, args).getRemainingArgs();

        // Identify the JAR file to replicate to all machines.
        job.setJarByClass(SessionStatisticsJobChainDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf1.set("mapreduce.user.classpath.first", "true");

        // Specify the input schema
        AvroJob.setInputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setInputValueSchema(job, Session.getClassSchema());

        // Specify the map
        job.setInputFormatClass(AvroKeyValueInputFormat.class);
        job.setMapperClass(UserSessionsFilterMapper.class);
        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);
        AvroJob.setMapOutputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setMapOutputValueSchema(job, Session.getClassSchema());

        // Set up multiple outputs
        AvroMultipleOutputs.addNamedOutput(job, "sessionType", AvroKeyValueOutputFormat.class, Schema.create(Schema.Type.STRING), Session.getClassSchema());
        AvroMultipleOutputs.setCountersEnabled(job, true);

        // Grab the input file and output directory from the command line.
        String[] inputPaths = appArgs[0].split(",");
        for (String inputPath : inputPaths) {
            FileInputFormat.addInputPath(job, new Path(inputPath));
        }

        FileOutputFormat.setOutputPath(job, new Path(appArgs[1]));
        job.waitForCompletion(true);
        /* END****************************** Binning Job ******************************END */

        /* The directory where our bins live */
        String binsDirectory = appArgs[1] + "/";

        /* Declare the output directories for the parallel jobs and the aggregator job */
        String clickerOutput = binsDirectory + "clickerStats";
        String submitterOutput = binsDirectory + "submitterStats";
        String sharerOutput = binsDirectory + "sharerStats";
        String aggregatorOutput = binsDirectory + "aggregatredStats";

        /* START****************************** Parallel job 1 - Clicker ******************************START */
        Configuration conf2 = getConf();
        Job clickerJob = new Job(conf2, "clickerStats");

        // Identify the JAR file to replicate to all machines.
        clickerJob.setJarByClass(SessionStatisticsJobChainDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf2.set("mapreduce.user.classpath.first", "true");

        // Specify the input schema
        clickerJob.setInputFormatClass(AvroKeyValueInputFormat.class);
        AvroJob.setInputKeySchema(clickerJob, Schema.create(Schema.Type.STRING));
        AvroJob.setInputValueSchema(clickerJob, Session.getClassSchema());

        // Specify the map
        clickerJob.setMapperClass(ClickStatisticsMapper.class);
        AvroJob.setMapOutputKeySchema(clickerJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setMapOutputValueSchema(clickerJob, ClickSubtypeStatisticsData.getClassSchema());

        // Specify the output type
        clickerJob.setOutputFormatClass(AvroKeyValueOutputFormat.class);

        // Specify the reduce
        clickerJob.setReducerClass(ClickStatisticsReducer.class);
        AvroJob.setOutputKeySchema(clickerJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setOutputValueSchema(clickerJob, ClickSubtypeStatisticsData.getClassSchema());

        FileInputFormat.addInputPath(clickerJob, new Path(binsDirectory + "Clicker*.avro"));

        FileOutputFormat.setOutputPath(clickerJob, new Path(clickerOutput));

        clickerJob.submit();
        /* END****************************** Parallel job 1 - Clicker ******************************END */

        /* START****************************** Parallel job 2 - Submitter ******************************START */
        Configuration conf3 = getConf();
        Job submitterJob = new Job(conf3, "submitterStats");

        // Identify the JAR file to replicate to all machines.
        submitterJob.setJarByClass(SessionStatisticsJobChainDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf3.set("mapreduce.user.classpath.first", "true");

        // Specify the input schema
        submitterJob.setInputFormatClass(AvroKeyValueInputFormat.class);
        AvroJob.setInputKeySchema(submitterJob, Schema.create(Schema.Type.STRING));
        AvroJob.setInputValueSchema(submitterJob, Session.getClassSchema());

        // Specify the map
        submitterJob.setMapperClass(ClickStatisticsMapper.class);
        AvroJob.setMapOutputKeySchema(submitterJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setMapOutputValueSchema(submitterJob, ClickSubtypeStatisticsData.getClassSchema());

        // Specify the output type
        submitterJob.setOutputFormatClass(AvroKeyValueOutputFormat.class);

        // Specify the reduce
        submitterJob.setReducerClass(ClickStatisticsReducer.class);
        AvroJob.setOutputKeySchema(submitterJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setOutputValueSchema(submitterJob, ClickSubtypeStatisticsData.getClassSchema());

        FileInputFormat.addInputPath(submitterJob, new Path(binsDirectory + "Submitter*.avro"));

        FileOutputFormat.setOutputPath(submitterJob, new Path(submitterOutput));

        submitterJob.submit();
        /* END****************************** Parallel job 2 - Submitter ******************************END */

        /* START****************************** Parallel job 3 - Sharer ******************************START */
        Configuration conf4 = getConf();
        Job sharerJob = new Job(conf4, "sharerStats");

        // Identify the JAR file to replicate to all machines.
        sharerJob.setJarByClass(SessionStatisticsJobChainDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf4.set("mapreduce.user.classpath.first", "true");

        // Specify the input schema
        sharerJob.setInputFormatClass(AvroKeyValueInputFormat.class);
        AvroJob.setInputKeySchema(sharerJob, Schema.create(Schema.Type.STRING));
        AvroJob.setInputValueSchema(sharerJob, Session.getClassSchema());

        // Specify the map
        sharerJob.setMapperClass(ClickStatisticsMapper.class);
        AvroJob.setMapOutputKeySchema(sharerJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setMapOutputValueSchema(sharerJob, ClickSubtypeStatisticsData.getClassSchema());

        // Specify the output type
        sharerJob.setOutputFormatClass(AvroKeyValueOutputFormat.class);

        // Specify the reduce
        sharerJob.setReducerClass(ClickStatisticsReducer.class);
        AvroJob.setOutputKeySchema(sharerJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setOutputValueSchema(sharerJob, ClickSubtypeStatisticsData.getClassSchema());

        FileInputFormat.addInputPath(sharerJob, new Path(binsDirectory + "Sharer*.avro"));
        FileOutputFormat.setOutputPath(sharerJob, new Path(sharerOutput));

        sharerJob.submit();
        /* END****************************** Parallel job 3 - Sharer ******************************END */

        /* Wait for all the parallel jobs to finish */
        while(!(clickerJob.isComplete() && submitterJob.isComplete() && sharerJob.isComplete())) { Thread.sleep(10000); }

        /* START****************************** Aggregator Job ******************************START */
        Configuration conf5 = getConf();
        Job aggregatorJob = new Job(conf5, "sharerStats");

        // Identify the JAR file to replicate to all machines.
        aggregatorJob.setJarByClass(SessionStatisticsJobChainDriver.class);
        // Use this JAR first in the classpath (We also set a bootstrap script in AWS)
        conf5.set("mapreduce.user.classpath.first", "true");


        // Specify the input schema
        aggregatorJob.setInputFormatClass(AvroKeyValueInputFormat.class);
        AvroJob.setInputKeySchema(aggregatorJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setInputValueSchema(aggregatorJob, ClickSubtypeStatisticsData.getClassSchema());


        // Specify the map
        aggregatorJob.setMapperClass(SessionStatsAggregatorMapper.class);
        AvroJob.setMapOutputKeySchema(aggregatorJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setMapOutputValueSchema(aggregatorJob, ClickSubtypeStatisticsData.getClassSchema());

        // Specify the output type
        aggregatorJob.setOutputFormatClass(TextOutputFormat.class);

        // Specify the reduce
        aggregatorJob.setReducerClass(ClickStatisticsReducer.class);
        AvroJob.setOutputKeySchema(aggregatorJob, ClickSubtypeStatisticsKey.getClassSchema());
        AvroJob.setOutputValueSchema(aggregatorJob, ClickSubtypeStatisticsData.getClassSchema());

        FileInputFormat.addInputPath(aggregatorJob, new Path(clickerOutput + "/part-r*.avro"));
        FileInputFormat.addInputPath(aggregatorJob, new Path(submitterOutput + "/part-r*.avro"));
        FileInputFormat.addInputPath(aggregatorJob, new Path(sharerOutput + "/part-r*.avro"));
        FileOutputFormat.setOutputPath(aggregatorJob, new Path(aggregatorOutput));

        aggregatorJob.waitForCompletion(true);
        /* END****************************** Aggregator Job ******************************END */

        return 0;
    }

    private static void printClassPath() {
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        URL[] urls = ((URLClassLoader) cl).getURLs();
        System.out.println("classpath BEGIN");
        for (URL url : urls) {
            System.out.println(url.getFile());
        }
        System.out.println("classpath END");

    }


    /**
     * The main method specifies the characteristics of the map-reduce job
     * by setting values on the Job object, and then initiates the map-reduce
     * job and waits for it to complete.
     */
    public static void main(String[] args) throws Exception {
        printClassPath();
            /* Purpose: custom args here */
            /* Configuration object created will be used in run. */
            /* new WordStatistics object will get run called on it with args */
            /* args are the parameters sent in from the cluster def */
        int res = ToolRunner.run(new Configuration(), new SessionStatisticsJobChainDriver(), args);
        System.exit(res);

    }
}
